/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sezal@andrew.cmu.edu, gdubey@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <ros/ros.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

bool should_inverse;
bool stabilized;

void OdomCallback(const nav_msgs::Odometry::ConstPtr& odom_msg) {
	static tf::TransformBroadcaster br;
	static tf::Transform odomTF;

  odomTF.setOrigin(tf::Vector3(odom_msg->pose.pose.position.x,
                               odom_msg->pose.pose.position.y,
                               odom_msg->pose.pose.position.z) );

  std::string child_frame_id = odom_msg->child_frame_id;

  if(stabilized)
  {
      odomTF.setRotation(
                  tf::createQuaternionFromYaw(
                      tf::getYaw(odom_msg->pose.pose.orientation)));
      child_frame_id = child_frame_id+"_stabilized";
  }
  else
  {
      odomTF.setRotation(tf::Quaternion(odom_msg->pose.pose.orientation.x,
                                        odom_msg->pose.pose.orientation.y,
                                        odom_msg->pose.pose.orientation.z,
                                        odom_msg->pose.pose.orientation.w));
  }

	if (should_inverse) {
    br.sendTransform(tf::StampedTransform(odomTF.inverse(),
                                          odom_msg->header.stamp,
                                          child_frame_id,
                                          odom_msg->header.frame_id));
  } else {
    br.sendTransform(tf::StampedTransform(odomTF, odom_msg->header.stamp,
                                          odom_msg->header.frame_id,
                                          child_frame_id));
  }

}

int main(int argc, char **argv) {
	ros::init(argc, argv, "odom_listener");
	ros::NodeHandle n;
	ros::NodeHandle np("~");
	ros::Subscriber sub = n.subscribe("odom", 100, OdomCallback);

	np.param<bool>("inverse", should_inverse, false);
    np.param<bool>("stabilize", stabilized, false);

	ros::spin();

	return 0;
}
